import myo

m = myo.Myo()



def proc_emg(emg, moving, times=[]):
	#print(emg)
	print(moving)

def proc_imu(quat, acc, gyro, times=[]):
	print acc

m.add_emg_handler(proc_emg)
m.add_imu_handler(proc_imu)
m.connect()

try:
	while True:
		m.run()
		
except KeyboardInterrupt:
	pass
finally:
	m.disconnect()
	print()
