import myo
import sys
from threading import Thread
import time
import os


import config

lastLine = ""

#delete training data from last run
filelist = [ f for f in os.listdir("trainingsets") ]
for f in filelist:
    os.remove(f)

#exit process
def buildData(name):
	m = myo.Myo(sys.argv[1] if len(sys.argv) >= 2 else None)

	f = open("trainingsets/" + name, "a")

	#Callback for EMG data from Myo (8 words)
	def proc_emg(emg, moving, times=[]):
		global lastLine
		lastLine = str(emg)
		print(emg)

	#Callback for other motion data, including accelerometer and gycroscope 
	def proc_imu(quat, acc, gyro, times=[]): 
		global lastLine
		if lastLine and len(gyro) == 3:  
			f.write(lastLine + str(gyro ) + "\n")
			print(gyro)


	m.add_emg_handler(proc_emg)
	m.add_imu_handler(proc_imu)

	m.connect()

	for x in range(0, config.samples):
		m.run()
	m.disconnect()


for i in range(0,config.numGestures):
	thread = Thread(target = buildData, args = ("test" + str(i), ))
	thread.deamon = True
	thread.start()
	thread.join()
	print "Next gesture"
	raw_input("Press Enter to continue...")

