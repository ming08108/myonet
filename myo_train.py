import time
import os
import myo
import sys
import math

from pybrain.datasets import SupervisedDataSet
from pybrain.tools.shortcuts import buildNetwork
from pybrain.supervised.trainers import BackpropTrainer

import config

from gui import *


app = myGUI()

running_list = []

last_gyro = ()

def listdir_nohidden(path):
    for f in os.listdir(path):
        if not f.startswith('.'):
            yield f



#11 items - 8 muscle impulses, 3 gyros, 3 files (3 gestures)
data = SupervisedDataSet(11, config.numGestures)

#Todo: Do for all files 

index = 0; 

for file in listdir_nohidden('trainingsets'): 
	f = open(os.getcwd() + '/trainingsets/' + file, "r")
	print f.name

	for line in f:
		line = line.replace(" ","")
		line = line.replace(")(",",")
		line = line.replace("(","")
		line = line.replace(")","")
		print line
		datapoints = [int(x) for x in line.split(',')]
		result = [0] * (config.numGestures)
		
		#if(index < len(result)): 
		result[index] = 1


		print result
		print datapoints

		data.addSample(datapoints, result)

	print "next"
	index = index + 1
	time.sleep(0.1)


net = buildNetwork(11, 100, config.numGestures)
trainer = BackpropTrainer(net, data)

print trainer.trainEpochs(epochs=4)


m = myo.Myo()

def proc_emg(emg, moving, times=[]):

	global last_gyro
	array = net.activate(emg + last_gyro)

	last_gyro = ()

	global running_list
	if(len(running_list) < 5): 
		running_list.append(find_largest(array))
	else: 
		app.setText(config.result_array[find_common(running_list)])
		running_list = []

def proc_imu(quat, accel, gyro, times=[]): 
	global last_gyro
	last_gyro = gyro


m.add_imu_handler(proc_imu)
m.add_emg_handler(proc_emg)

m.connect()

def find_common(result_list): 
	count_array = [0] * config.numGestures

	for index in result_list: 
		count_array[index] = count_array[index] + 1

	return find_largest(count_array) 


def find_largest(array): 	
	highest = -1
	index = 0

	for x in range(0, len(array)):  
		if(highest < array[x]): 
			highest = array[x]
			index = x

	return index

try:
	while True:
		m.run()
		
except KeyboardInterrupt:
	pass
finally:
	m.disconnect()
	print()


