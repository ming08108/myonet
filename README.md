#MyoNet - Custom Gesture recognition through deep neural net learning

##How it works 

Series of python scripts used to capture and classify custom gestures obtained from the Myo Raw Library using neural networks


##Dependencies

* MyoRaw and associated dependencies
* PyBrain

#License

MIT